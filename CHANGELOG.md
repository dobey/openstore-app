# OpenStore App Changelog

## v3.2.11

- Fixed broken app when there are too many apps installed
- Fixed tap area for filters
- Various bug fixes

## v3.2.10

- Fixed install progress bar not showing

## v3.2.9

- Fixed crash when still using OTA-15

## v3.2.8

- Visual tweaks
- Fixed author searches not opening in the app correctly
- Translation update, thank you translators!

## v3.2.7

- Translation update, thank you translators!

## v3.2.6

- Translation update, thank you translators!

## v3.2.5

- Updated translations, thank you translators!

## v3.2.4

- Updated translations, thank you translators!

## v3.2.3

- Updated translations, thank you translators!

## v3.2.2

- Updated translations, thank you translators!

## v3.2.1

- Fixed typo

## v3.2.0

- Added sorting apps
- Added filtering apps by type

## v3.1.6

- Updated translations, thank you translators!

## v3.1.5

- Updated translations, thank you translators!

## v3.1.4

- Updated translations, thank you translators!

## v3.1.3

- New splash screen
